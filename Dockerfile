FROM purban/centos7_java8

RUN yum install -y tomcat \
    && yum clean all

ONBUILD ENV DEPLOYDIR /usr/share/tomcat/webapps
ONBUILD ENV SRC src
ONBUILD ADD $SRC $DEPLOYDIR

EXPOSE 8080

ENTRYPOINT [ "bash" ]

CMD [ "/usr/libexec/tomcat/server" , "start"  ] 

